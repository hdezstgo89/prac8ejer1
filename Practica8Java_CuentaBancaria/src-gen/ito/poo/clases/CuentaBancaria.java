package ito.poo.clases;

import java.time.LocalDate;

public class CuentaBancaria {

	private long numCuenta = 0L;
	private String nomCliente = "";
	private float saldo = 0F;
	private LocalDate fechaApertura = null;
	private LocalDate fechaActualizacion = null;
	/*********************/
	public CuentaBancaria() {
		super();
	}
	public CuentaBancaria(long numCuenta, String nomCliente, float saldo, LocalDate fechaApertura) {
		super();
		this.numCuenta = numCuenta;
		this.nomCliente = nomCliente;
		this.saldo = saldo;
		this.fechaApertura = fechaApertura;
	}
	/*********************/
	public boolean Deposito(float Cantidad,LocalDate newfechaActualizacion) {
		boolean Deposito = false;
		if(this.fechaApertura==null)
			System.out.println("La cuenta no esta activa");
		else {
			Deposito = true;
			this.setSaldo(this.getSaldo()+ Cantidad);
			this.setFechaActualizacion(newfechaActualizacion);
		}
		return Deposito;
	}

	public boolean Retiro (float Cantidad,LocalDate newFechaActualizacion) {
		boolean Retiro = false;
		if (Cantidad <= this.getSaldo()) {
			 Retiro=true;
			 this.setSaldo(this.getSaldo()-Cantidad);
			 this.setFechaActualizacion(newFechaActualizacion);
			}
		else
			System.out.println("La cantidad a retirar sobrepasa el saldo");
		return Retiro;
	}
	/*********************/
	public long getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(long numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getNomCliente() {
		return nomCliente;
	}

	public float getSaldo() {
		return saldo;
	}

	public void setSaldo(float saldo) {
		this.saldo = saldo;
	}

	public LocalDate getFechaApertura() {
		return fechaApertura;
	}

	public void setFechaApertura(LocalDate fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public LocalDate getFechaActualizacion() {
		return fechaActualizacion;
	}

	public void setFechaActualizacion(LocalDate fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	/*********************/
	@Override
	public String toString() {
		return "CuentaBancaria [numCuenta=" + numCuenta + ", nomCliente=" + nomCliente + ", saldo=" + saldo
				+ ", fechaApertura=" + fechaApertura + ", fechaActualizacion=" + fechaActualizacion + "]";
	}
	/*********************/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fechaActualizacion == null) ? 0 : fechaActualizacion.hashCode());
		result = prime * result + ((fechaApertura == null) ? 0 : fechaApertura.hashCode());
		result = prime * result + ((nomCliente == null) ? 0 : nomCliente.hashCode());
		result = prime * result + (int) (numCuenta ^ (numCuenta >>> 32));
		result = prime * result + Float.floatToIntBits(saldo);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CuentaBancaria other = (CuentaBancaria) obj;
		if (fechaActualizacion == null) {
			if (other.fechaActualizacion != null)
				return false;
		} else if (!fechaActualizacion.equals(other.fechaActualizacion))
			return false;
		if (fechaApertura == null) {
			if (other.fechaApertura != null)
				return false;
		} else if (!fechaApertura.equals(other.fechaApertura))
			return false;
		if (nomCliente == null) {
			if (other.nomCliente != null)
				return false;
		} else if (!nomCliente.equals(other.nomCliente))
			return false;
		if (numCuenta != other.numCuenta)
			return false;
		if (Float.floatToIntBits(saldo) != Float.floatToIntBits(other.saldo))
			return false;
		return true;
	}
	public int compareTo(CuentaBancaria arg0) {
		int r=0;
		if(this.numCuenta != arg0.getNumCuenta())
			return this.numCuenta>arg0.getNumCuenta()?1:-1;
		else if (this.saldo != arg0.getSaldo())
			return this.saldo>arg0.getSaldo()?2:-2;
		else if (!this.nomCliente.equals(arg0.getNomCliente()))
				return this.nomCliente.compareTo(arg0.getNomCliente());
		else if (!this.fechaApertura.equals(arg0.getFechaApertura()))
			return this.fechaApertura.compareTo(arg0.getFechaApertura());
		return r;
	}

}
